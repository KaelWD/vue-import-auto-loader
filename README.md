# vue-import-auto-loader

[webpack](https://webpack.js.org/) loader to automatically import [vue](https://vuejs.org) components as they're used

```js
// webpack.config.js

exports.module.rules = [{
  test: /\.vue$/,
  use: [
    {
      loader: 'vue-import-auto-loader',
      options: {
        /**
         * This function will be called for every tag used in each vue component
         * It should return an array, the first element will be inserted into the
         * components array, the second should be a corresponding import
         *
         * originalTag - the tag as it was originally used in the template
         * kebabTag    - the tag normalised to kebab-case
         * camelTag    - the tag normalised to PascalCase
         * path        - a relative path to the current .vue file
         * component   - a parsed representation of the current component
         */
        match (originalTag, { kebabTag, camelTag, path, component }) {
          if (kebabTag.startsWith('core-')) {
            return [camelTag, `import ${camelTag} from '@/components/core/${camelTag.substring(4)}.vue'`]
          }
        },
        /**
         * The loader also ships with some preset matchers
         */
        preset: 'vuetify'
      }
    },
    'vue-loader'
  ],
  exclude: /node_modules/
}]
```

```html
<template>
  <core-form>
    <v-card>
      ...
    </v-card>
  </core-form>
</template>

<script>
  export default {
    ...
  }
</script>
```

Will be compiled into:

```html
<template>
  <core-form>
    <v-card>
      ...
    </v-card>
  </core-form>
</template>

<script>
  import { VCard } from 'vuetify/es5/components/VCard'
  import CoreForm from '@/components/core/Form.vue'

  export default {
    components: {
      VCard,
      CoreForm
    },
    ...
  }
</script>
```
