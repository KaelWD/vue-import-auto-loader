const componentMap = require('./generator')

module.exports = {
  match (_, { kebabTag, camelTag: tag }) {
    if (!kebabTag.startsWith('v-')) return

    const component = componentMap.find(val => val.component === tag)
    if (component) return [tag, `import { ${tag} } from 'vuetify/es5/components/${component.group}'`]
  }
}
