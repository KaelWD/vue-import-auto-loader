const loaderUtils = require('loader-utils')
const compiler = require('vue-template-compiler')
const { camelize, capitalize, hyphenate } = require('./util')

module.exports = function (content, sourceMap) {
  this.cacheable()

  const options = {
    match: [],
    ...loaderUtils.getOptions(this)
  }

  options.match = options.match || []
  if (!Array.isArray(options.match)) options.match = [options.match]

  if (options.preset && !Array.isArray(options.preset)) options.preset = [options.preset]
  if (options.preset) {
    presets = options.preset.map(preset => require('./presets/' + preset).match)
    options.match = options.match.concat(...presets)
  }

  if (!this.resourceQuery) {
    this.addDependency(this.resourcePath)

    const tags = new Set()
    const file = this.fs.readFileSync(this.resourcePath).toString('utf8')
    const component = compiler.parseComponent(file)
    compiler.compile(component.template.content, {
      modules: [{
        postTransformNode: node => { tags.add(node.tag) }
      }]
    })

    const imports = []
    const components = []
    tags.forEach(tag => {
      for (const matcher of options.match) {
        const match = matcher(tag, {
          kebabTag: hyphenate(tag),
          camelTag: capitalize(camelize(tag)),
          path: this.resourcePath.substring(this.rootContext.length + 1),
          component
        })
        if (match) {
          components.push(match[0])
          imports.push(match[1])
          break
        }
      }
    })

    if (imports.length) {
      content += '\n\n/* vue-auto-loader */\n'
      imports.forEach(i => {
        content += i + '\n'
      })
      content += `component.options.components = Object.assign({\n  ${components.join(',\n  ')}\n}, component.options.components)`
    }
  }

  this.callback(null, content, sourceMap)
}
